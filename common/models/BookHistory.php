<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "book_history".
 *
 * @property int $id
 * @property int $book_id
 * @property int $user_id
 * @property int $status
 * @property string $datetime
 *
 * @property Books $book
 * @property User $user
 */
class BookHistory extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_ADMIN_DELETED = 2;
    const STATUS_USER_DELETED = 3;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'book_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['book_id', 'user_id', 'status', 'datetime'], 'required'],
            [['book_id', 'user_id', 'status'], 'integer'],
            [['datetime'], 'safe'],
            //[['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => Books::className(), 'targetAttribute' => ['book_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'book_id' => 'Book ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'datetime' => 'Datetime',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBook()
    {
        return $this->hasOne(Book::className(), ['id' => 'book_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function saveHistory0($id){

        $this->book_id = $id;
        $this->user_id = Yii::$app->user->identity->id;
        $this->status = self::STATUS_INACTIVE;
        $this->datetime = date('Y-m-d H:i:s');

         return $this->save();
    }

    public function saveHistory1($idBook){

        $this->book_id = $idBook->book_id;
        $this->user_id = $idBook->user_id;
        $this->status = self::STATUS_ACTIVE;
        $this->datetime = date('Y-m-d H:i:s');

         return $this->save();
    }

    public function saveHistory2($model){

        $this->book_id = $model->book_id;
        $this->user_id = $model->user_id;
        $this->status = self::STATUS_ADMIN_DELETED;
        $this->datetime = date('Y-m-d H:i:s');

         return $this->save();
    }

    public function saveHistory3($model){

        $this->book_id = $model->book_id;
        $this->user_id = $model->user_id;
        $this->status = self::STATUS_USER_DELETED;
        $this->datetime = date('Y-m-d H:i:s');

         return $this->save();
    }


}
