<?php

namespace common\models;

use Yii;
use yii\helpers\Html;
use common\models\BookTenant;

/**
 * This is the model class for table "books".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $date
 * @property int $status
 */
class Book extends \yii\db\ActiveRecord
{
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'date', 'status'], 'required'],
            [['name', 'description'], 'string'],
            [['date'], 'safe'],
            [['status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'date' => 'Date',
            'status' => 'Status',
        ];
    }


    public function buttonAdmin($idLease){

        //$book = new Book();

        if(BookTenant::find()->where(['status' => 0])->andWhere(['id' => $idLease])->one()){
                    $button = Html::a('Выдать', ['book/issue', 'id' => $idLease], ['class' => 'btn btn-primary']);
                }elseif(BookTenant::find()->where(['status' => 1])->andWhere(['id' => $idLease ])->one()){
                    $button = Html::a('Вернуть', ['book/book_delete', 'id' => $idLease], ['class' => 'btn btn-danger']);
                }

        return $button;
    }
}
