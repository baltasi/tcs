<?php

namespace common\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "book_tenant".
 *
 * @property int $id
 * @property int $book_id
 * @property int $user_id
 * @property int $status
 *
 * @property Books $book
 * @property User $user
 */
class BookTenant extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'book_tenant';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['book_id', 'user_id'], 'required'],
            [['book_id', 'user_id', 'status'], 'integer'],
          //  [['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => Books::className(), 'targetAttribute' => ['book_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'book_id' => 'Book ID',
            'user_id' => 'User ID',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBook()
    {
        return $this->hasOne(Book::className(), ['id' => 'book_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function saveBook($id){

        $bookTenant = new BookTenant();


        $this->book_id = $id;
        $this->user_id = Yii::$app->user->identity->id;
        
         return $this->save();
    }


    public function issueBook($id){

        $bookTenant = new BookTenant();

        $this->status = 1;
        
         return $this->insert();
    } 

    public function button($id){

        if(!empty(BookTenant::find()->where(['user_id' => Yii::$app->user->identity->id])->andWhere(['book_id' => $id])->one())){
                if(BookTenant::find()->where(['user_id' => Yii::$app->user->identity->id])->andWhere(['book_id' => $id])->andWhere(['status' => 0])->one()){
                    $button = Html::a('Обработке/Отменить', ['cancel', 'id' => $id], ['class' => 'btn btn-info']);
                }elseif(BookTenant::find()->where(['user_id' => Yii::$app->user->identity->id])->andWhere(['book_id' => $id])->andWhere(['status' => 1])->one()){
                    $button = Html::a('Вернуть', ['cancel', 'id' => $id], ['class' => 'btn btn-danger']);
                }
        }else{
            $button = Html::a('Арнедововать', ['occupy', 'id' => $id], ['class' => 'btn btn-primary']);
        }

        return $button;
    }

   


    
}
