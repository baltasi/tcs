<?php

if(!Yii::$app->user->isGuest){
    $only = Yii::$app->user->identity->id;
}else{
    $only = "";
}

if(!Yii::$app->user->isGuest){
    $username = Yii::$app->user->identity->username;
}else{
    $username = "";
}



?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= $username ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Меню для клиентам', 'options' => ['class' => 'header']],
                    ['label' => 'Профиль', 'icon' => 'users-code-o', 'url' => ['user/update/'. $only]],
                    ['label' => 'Книги', 'icon' => 'file-code-o', 'url' => ['/book/list_book']],
                    ['label' => 'Мои книги', 'icon' => 'users-code-o', 'url' => ['/book/my_books']],
                    ['label' => 'Меню для админу', 'options' => ['class' => 'header']],
                    ['label' => 'Пользователи', 'icon' => 'users-code-o', 'url' => ['/user']],
                    ['label' => 'Книги', 'icon' => 'file-code-o', 'url' => ['/book']],
                    ['label' => 'Заказзы', 'icon' => 'users-code-o', 'url' => ['/book/lease']],
                    ['label' => 'История пользователей', 'icon' => 'users-code-o', 'url' => ['/book/history']],
                    
                    
                ],
            ]
        ) ?>

    </section>

</aside>
