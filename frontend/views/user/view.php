<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'auth_key',
            'password_hash',
            'password_reset_token',
            'email:email',
            'status',
            'created_at',
            'updated_at',
            'verification_token',
        ],
    ]) ?>

    <table border="1">
   <caption>Аренда книги</caption>
    <tr>
        <th style="width: 55px">Имя</th>
        <th style="width: 1050px">Книга</th>
        <th>История</th>
        <th style="width: 108px">Дата</th>
    </tr>
        <?php foreach ($bookHistory as $bookStory): ?>
        <tr>
            <td><?= $bookStory->user->username ?></td>
            <td><?= $bookStory->book->name ?></td>
            <td>
                <?php if($bookStory->status == 0): ?>
                    <?= "Запросил" ?>
                <?php elseif ($bookStory->status == 1): ?>
                    <?= "Админ разрешил" ?>
                <?php elseif ($bookStory->status == 2):?>
                    <?= "Админ взял обратно" ?>
                <?php elseif($bookStory->status == 3): ?>
                    <?= "Пользователь отменил заказ" ?>
                <?php endif; ?>
            </td>
            <td><?=$bookStory->datetime?></td>
        </tr>
        <?php endforeach; ?>
</table>

</div>
