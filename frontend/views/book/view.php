<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Book */

$this->title = $model->name;
$id = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="book-view">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <p>
        <?php if (Yii::$app->user->can('admin')): ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?php else: ?>
        
        <?= $bookTenant->button($id); ?>
    <?php endif ?>
    </p>

    <?php if(empty($description)): ?>
     <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name:ntext',
        ],
    ]) ?>
    <?php else: ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name:ntext',
            'description:ntext',
            'date',
            'status',
        ],
    ]) ?>
<?php endif ?>

</div>
