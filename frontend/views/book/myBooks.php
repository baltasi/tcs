<?php

use Yii;
use yii\helpers\Url;
?>

<style type="text/css">
table {
font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
text-align: left;
border-collapse: separate;
border-spacing: 5px;
background: #ECE9E0;
color: #656665;
border: 16px solid #ECE9E0;
border-radius: 20px;
width: -moz-available;
}
th {
font-size: 18px;
padding: 10px;
}
td {
background: #F5D7BF;
padding: 10px;
}
</style>


<table>
   <caption>Мои книги</caption>
<tr>
  <th>№</th>
  <th colspan="1">Изображение</th>
  <th colspan="2">Название книг</th>
</tr>
<?php  $i = 1; foreach ($leases as $lease): ?>
<tr>  
  <td><?= $i ?></td>
  <td><img src="/images/index.png"></td>
  <td style="font-size: x-large;"><a href="<?= Url::toRoute(['book/view', 'id' => $lease->book->id]) ?>"><?= $lease->book->name ?></a></td>
</tr>
<?php $i++; endforeach; ?>
</table>