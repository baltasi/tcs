<table border="1">
   <caption>История книг</caption>
    <tr>
        <th style="width: 55px">Имя</th>
        <th style="width: 1050px">Книга</th>
        <th>История</th>
        <th style="width: 108px">Дата</th>
    </tr>
        <?php foreach ($history as $bookStory): ?>
        <tr>
            <td><?= $bookStory->user->username ?></td>
            <td><?= $bookStory->book->name ?></td>
            <td>
                <?php if($bookStory->status == 0): ?>
                    <?= "Запросил" ?>
                <?php elseif ($bookStory->status == 1): ?>
                    <?= "Админ разрешил" ?>
                <?php elseif ($bookStory->status == 2):?>
                    <?= "Админ взял обратно" ?>
                <?php elseif($bookStory->status == 3): ?>
                    <?= "Пользователь отменил заказ" ?>
                <?php endif; ?>
            </td>
            <td><?=$bookStory->datetime?></td>
        </tr>
        <?php endforeach; ?>
</table>