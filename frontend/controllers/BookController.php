<?php

namespace frontend\controllers;

use Yii;
use common\models\Book;
use common\models\BooksSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\BookHistory;
use common\models\BookTenant;
use yii\filters\AccessControl;

/**
 * BookController implements the CRUD actions for Book model.
 */
class BookController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],

                    ],
                    [
                        'actions' => ['view','occupy','my_books','cancel','list_book'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['logout','create','delete','update','lease','book_delete','issue','index','history'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Book models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BooksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Book model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $book = new Book();
        $bookTenant = new BookTenant();
        $description = BookTenant::find()->where(['user_id' => Yii::$app->user->identity->id])->andWhere(['book_id' => $id])->andWhere(['status' => 1])->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'book' => $book,
            'bookTenant' => $bookTenant,
            'description' => $description,
        ]);
    }

    /**
     * Creates a new Book model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Book();

        $model->status = 1;
        $model->date = date('Y-m-d H:i:s');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            ]);
    }

/**
* Updates an existing Book model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id
* @return mixed
* @throws NotFoundHttpException if the model cannot be found
*/
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Book model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Book::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionOccupy($id){
        
        $bookHistory = new BookHistory();
        $bookTenant = new BookTenant();

        $bookHistory->saveHistory0($id);
        $bookTenant->saveBook($id);

        return $this->redirect(['view','id' => $id]);
        
    }

    public function actionLease(){
        
        $book = new Book();
        
        $leases = BookTenant::find()->all();

        return $this->render('lease',[
            'leases' => $leases,
            'book' => $book,
        ]);
    }

    public function actionIssue($id){
        
        $bookHistory = new BookHistory();

        $model = BookTenant::findOne($id);
        $model->status = 1;
        $model->save();  // equivalent to $model->update();

        $idBook = $model;

        $bookHistory->saveHistory1($idBook);

        return $this->redirect(['lease']);
        
    }

    public function actionBook_delete($id){


        $bookHistory = new BookHistory();
        $model = BookTenant::findOne($id);
        $bookHistory->saveHistory2($model);
        
        $model = BookTenant::findOne($id)->delete();

        return $this->redirect(['lease']);
        
    }

    public function actionCancel($id){

        $idTenant = BookTenant::find()->where(['book_id' => $id])->andWhere(['user_id' => Yii::$app->user->identity->id])->one();

        $bookHistory = new BookHistory();
        $model = BookTenant::findOne($idTenant->id);
        $bookHistory->saveHistory3($model);
        
        $model = BookTenant::findOne($idTenant->id)->delete();

        return $this->redirect(['view','id' => $id]);
        
    }

    public function actionMy_books(){
        $book = new BookTenant();
        

        $leases = BookTenant::find()->where(['user_id' => Yii::$app->user->identity->id])->andWhere(['status' => 1])->all();

        return $this->render('myBooks',[
            'leases' => $leases,
            'book' => $book,
        ]);
    }

    public function actionList_book(){
        //$book = new BookTenant();

        $listB = Book::find()->all(); 
        return $this->render('listBook',[
            'listB' => $listB,
        ]);
    }

    public function actionHistory(){
        $history = BookHistory::find()->all();

        return $this->render('history',[
            'history' => $history,
        ]);
    }

}
